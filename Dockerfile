FROM openjdk:11-jdk-slim

WORKDIR /app

COPY /build/libs/spring-boot-example-app-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT java -jar app.jar
